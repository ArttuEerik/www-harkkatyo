<!DOCTYPE html>

<html>
    <head>
      <title>SummerCottages Co. Log out</title>

      <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
      <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
      <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>

      <link type="text/css" rel="stylesheet" href="stylesheet.css" />
      <script type="text/javascript" src="script.js"></script>
    </head>
    
    <body>
 
        <?php
        session_start();
        $_SESSION = array();
        if (ini_get("session.use_cookies")) {
            $params = session_get_cookie_params();
            setcookie(session_name(), '', time() - 42000,
            $params["path"], $params["domain"],
            $params["secure"], $params["httponly"]
            );
        }
        session_destroy();
        ?>
        
        <div id="pageTitle">
            <h1 id="company">SummerCottages Co.</h1>
        </div>
        <div class="divider">Sign out</div>
        <p align="center">You have logged out. Have a nice day!</p>
        <p align="center"><a href="https://www-harjoitustyo-thearthur.c9users.io/login.php">Back to log in</a></p>
        <p align="center">Log out of facebook here:
            <fb:login-button scope="public_profile,email" data-auto-logout-link="true" onlogin="checkLoginState();">
            </fb:login-button>
        </p>
        
    </body>
</html>