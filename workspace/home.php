<!DOCTYPE html>



<?php 
  
  session_start();
  if(isset($_SESSION['username'])) {
?>  


<html>
<head>
  
  <meta charset="utf=8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  
  <title>SummerCottages Co. Home</title>
  
  <!-- Needed links: ajax, stylesheet, js etc.-->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
  <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>

  <!-- links to css and js -->
  <link type="text/css" rel="stylesheet" href="stylesheet.css" />
  <script type="text/javascript" src="script.js"></script>

</head>
<body>
  
  <!-- Page title -->
  <div id="pageTitle">
    <h1 id="company">SummerCottages Co.</h1>
    
  </div>

<!-- Navigation bar -->
  <div id="navbar">
    <div id="wrapper">
      <ul class="nav">
        <li><a href="https://www-harjoitustyo-thearthur.c9users.io/home.php" class="active">Home</a></li>
        <li><a href="https://www-harjoitustyo-thearthur.c9users.io/make.php">Make reservation</a></li>
        <li><a href="https://www-harjoitustyo-thearthur.c9users.io/weather.php">Weather</a></li>
        <li><a href="https://www-harjoitustyo-thearthur.c9users.io/about.php">About</a></li>

        <li class="rightside"><a id="logout" href="https://www-harjoitustyo-thearthur.c9users.io/logout.php">Sign out</a></li>
        
      </ul>
    </div>
  </div>
  
  <!-- Content starts -->
  
  <img class="headImage" src="images/lake.jpg"></img>
  
  <p class="intro">Wellcome to SummerCottages Co. Internet reservations services. </p>
  <p>Via these websites you can easily make reserevations to our rental cottage. At the momen only 1 cottage is available.</p>
  <p>All your reservations are saved to database and you will be billed at the end of your vacation.</p>
  
  




</body>
</html>


<?php
}
else {
  
  header('Location: https://www-harjoitustyo-thearthur.c9users.io/login.php');
}
?>