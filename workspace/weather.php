<!DOCTYPE html>

<?php 
  session_start();
  if(isset($_SESSION['username'])) {
?>  

<html>
<head>
  
  <meta charset="utf=8">
  
  <title>SummerCottages Co. Weather</title>
  
  <!-- Needed links: ajax, stylesheet, js etc.-->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
  <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>

  <link type="text/css" rel="stylesheet" href="stylesheet.css" />
  <script type="text/javascript" src="script.js"></script>

</head>
<body>
  <!-- Page title -->
  <div id="pageTitle">
    <h1 id="company">SummerCottages Co.</h1>
    
  </div>


<!-- Navigation bar -->
  <div id="navbar">
    <div id="wrapper">
      <ul class="nav">
        <li><a href="https://www-harjoitustyo-thearthur.c9users.io/home.php">Home</a></li>
        <li><a href="https://www-harjoitustyo-thearthur.c9users.io/make.php">Make reservation</a></li>
        <li><a href="https://www-harjoitustyo-thearthur.c9users.io/weather.php" class="active">Weather</a></li>
        <li><a href="https://www-harjoitustyo-thearthur.c9users.io/about.php">About</a></li>
        <!-- <li style="float:right"><a href="">Sign in</a></li> -->
        <li  class="rightside"><a href="https://www-harjoitustyo-thearthur.c9users.io/logout.php">Sign out</a></li>
        
      </ul>
    </div>
  </div>
  
  <!-- Content starts -->
  
  <img class="headImage" src="images/sunset.jpg"></img>
  
  
  <div class="weather">
    <p class="describe">Here you can inspect current weather at our cottage locations.</p>
    <p class="describe">Currently available cottage:</p>
    <p class="describe">Log house ashore of Saimaa lake. Location: Imatra.</p>
  </div>
  
  <!-- Weather from target area -->
  <div class="weather">
      
    <?php
            // Setting the timezone
            date_default_timezone_set("Europe/Helsinki");
            
            $mem = new Memcached();
            $mem->addServer("127.0.0.1", 11211) or die("Unable to connect");
            
            $result = $mem->get("key");
            $mem->flush(3600);
            
            if ($result) {
              echo $result;
//	                echo "<p>Source: memcached</p>";
                }
                
            
                else {
                    $jsonData = file_get_contents("http://api.openweathermap.org/data/2.5/weather?q=Imatra,fi&APPID=dfd8305c4a0f74a86465bec8d4f24ad7");
                    $json = json_decode($jsonData,true);
                    $mainWeather = $json["weather"][0]["main"];
                    $temp = round($json["main"]["temp"] - 273.15, 2);
                    $city = $json["name"];
                    $date = $json["dt"];
                    $output = "<div id='city'>
                            <h2>City name: ".$city."</h2>
                            <ul>
                                <li>Weather type: ".$mainWeather."</li>
                                <li>Temperature: ".$temp." C</li>
                                <li>Data red: ".date("F j, Y, g:i a", $date)."</li>
                            </ul>
                            </div>";
            
	                $mem->set("key", $output);
	                /*
	                echo $output;
	                echo "<p>Source: API</p>";
	                */
                
                  header('Location: '.$_SERVER['REQUEST_URI']);
                }
                
            
        ?>
        
        
  </div>


</body>
</html>


<?php
}
else {
  
  header('Location: https://www-harjoitustyo-thearthur.c9users.io/login.php');
}
?>