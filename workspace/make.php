<!DOCTYPE html>

<?php 
  session_start();
  if(isset($_SESSION['username'])) {
?>  

<html>
<head>
  
  <meta charset="utf=8">

  <title>SummerCottages Co. Reservation</title>
  
  <!-- Needed links: ajax, stylesheet, js etc.-->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
  <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
  
  
    <!-- 3rd party calendar widget include    -->

    <link rel="stylesheet" href="availability-calendar/assets/style.css" />
    <link rel="stylesheet" href="availability-calendar/assets/dateTimePicker.css" />
    <script type="text/javascript" src="availability-calendar/scripts/components/jquery.min.js"></script>
    <script type="text/javascript" src="availability-calendar/scripts/dateTimePicker.min.js"></script>

    <link type="text/css" rel="stylesheet" href="stylesheet.css" />

  <!-- Not in use due calendar application. Necessary styling in availability-calendar/assets/style.css
  <link type="text/css" rel="stylesheet" href="stylesheet.css" />
  -->
  <script type="text/javascript" src="script.js"></script>
  

  
  
</head>

<body>
  <!-- Page title -->
  
  <div id="pageTitle">
    <h1 id="company">SummerCottages Co.</h1>
  </div>  
 


<!-- Navigation bar -->
  
  <div id="navbar">
    <div id="wrapper">
      <ul class="nav">
        <li><a href="https://www-harjoitustyo-thearthur.c9users.io/home.php">Home</a></li>
        <li><a href="https://www-harjoitustyo-thearthur.c9users.io/make.php" class="active">Make reservation</a></li>
        <li><a href="https://www-harjoitustyo-thearthur.c9users.io/weather.php">Weather</a></li>
        <li><a href="https://www-harjoitustyo-thearthur.c9users.io/about.php">About</a></li>
        <!-- <li style="float:right"><a href="">Sign in</a></li> -->
        <li  class="rightside"><a href="https://www-harjoitustyo-thearthur.c9users.io/logout.php">Sign out</a></li>
        
      </ul>
    </div>
  </div>
 
  
  <!-- Content starts -->
  
  <img class="headImage" src="images/era.jpg"></img>
  
  
  <p id="makeIntro"  class="intextlist">
   In this page you can make a reservation to our cottages in just 2 steps: 
  </p>   
  <p class="intextlist"><b>1.</b> Select dates you wish to use the cottage.</p>
  <p class="intextlist"><b>2.</b> Make a reservation.</p>
  
  
  <div class="unit">
    
    <p class="describe">Currently available cabin:</p>
    <p class="describe">Log house ashore of Saimaa lake. Location: Imatra.</p>
    
    
    <!-- reservation area -->
    <div class="divider">Make a reservation</div>
    
    <!-- How to use -->
    <div id="order">
      <div id="resText">
        <p> </p>
      <p>
        Please select days you wish to use the cottage. Press 'submit' when ready.
      </p>
      <p>Please note that red dates are already reserved. All reservations starts at 12:00 PM and ends at 10:00 AM.
      You can reserve up to 7 days period, and longer timeline will be ignored. Choosing reserved days will be ingored as well.
  You can have multiple reservations at the same time. Have a nice vacation!</p>
      <p>If calendar does not update after submitting, pleas reload the page.</p>
           
      </div>
    </div>
    
    
    
    <!-- 3rd party calendar here -->
    
    
    <div id="basic" data-toggle="calendar" data-month="12" data-year="2016" ></div>
    
    
   
    
    
  </div>

  
<!-- Choose dates of stay -->
  <form id="addNew" action="post.php" method="post">
    <div><p>From: <input type="date" name="start" id="start"/></p>
    <p>To: <input type="date" name="stop" id="stop"/></p></div>
    <input type="submit" name="add" value="Submit"/>
  </form> 
  
  
  
   
    
  <div class="unit">
    
    
 
    <div id=cottageDisplay>
    <div class="divider">Images of Log house: Imatra</div>  
      <img class="cabin" src="images/house/house.jpg"></img>
      <img class="cabin" src="images/house/inside.jpg"></img>
      <img class="cabin" src="images/house/sauna.jpg"></img>
      <img class="cabin" src="images/house/island.jpg"></img>
    </div>
  </div>
    
    
  
  
  
 

</body>
</html>

<!-- Calendar script here, beacuse other pages would get error message if this was in script.js

-->
<script type="text/javascript">
      
      
      // calendar creation
    
    $(document).ready(function(){
      $('#basic').calendar({
        day_first: 1,
        adapter: 'availability-calendar/server/adapter.php'
        
      });
      
    });
  
</script>


<?php
}
else {
  
  header('Location: https://www-harjoitustyo-thearthur.c9users.io/login.php');
}
?>