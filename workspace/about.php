<!DOCTYPE html>

<?php 
  session_start();
  if(isset($_SESSION['username'])) {
?>  


<html>
<head>
  
  <meta charset="utf=8">
  
  <title>SummerCottages Co. About</title>
  
  <!-- Needed links: ajax, stylesheet, js etc.-->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
  <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>

  <link type="text/css" rel="stylesheet" href="stylesheet.css" />
  <script type="text/javascript" src="script.js"></script>

</head>
<body>
  <!-- Page title -->
  <div id="pageTitle">
    <h1 id="company">SummerCottages Co.</h1>
    
  </div>


<!-- Navigation bar -->
  <div id="navbar">
    <div id="wrapper">
      <ul class="nav">
        <li><a href="https://www-harjoitustyo-thearthur.c9users.io/home.php">Home</a></li>
        <li><a href="https://www-harjoitustyo-thearthur.c9users.io/make.php">Make reservation</a></li>
        <li><a href="https://www-harjoitustyo-thearthur.c9users.io/weather.php">Weather</a></li>
        <li><a href="https://www-harjoitustyo-thearthur.c9users.io/about.php" class="active">About</a></li>
        <!-- <li style="float:right"><a href="">Sign in</a></li> -->
        <li  class="rightside"><a href="https://www-harjoitustyo-thearthur.c9users.io/logout.php">Sign out</a></li>
        
      </ul>
    </div>
  </div>
  
  <!-- Content starts -->
  
  <img class="headImage" src="images/sunset.jpg"></img>
  
  <p class="about">
    SummerCottages Co. is fictional corporation created for web-development cource at Lappeenranta University of Technology in winter 2016 by Arttu Ristola.
  </p>
  <p class="about">
    If by chance there is an actual company by the same name, this site has nothing to do with it. 
    All content in this site is made for learning purposes and do not represent any real life services whatsoever. 
  </p>

  <p class="about">
    If you have any questions or wish to contact the author, please contact via e-mail: <i>arttu.ristola@student.lut.fi</i>.
  </p>



</body>
</html>


<?php
}
else {
  
  header('Location: https://www-harjoitustyo-thearthur.c9users.io/login.php');
}
?>